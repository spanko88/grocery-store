export interface Ifood {
	id:number,
	value:string,
	label:string,
	priceUnit:number,
	sale?:number,
	priceSale?:number
}
export const food:Ifood[] = [
	{ id: 0, value: 'milk', label: 'Milk', priceUnit: 3.97, sale: 2, priceSale: 5 },
	{ id: 1, value: 'bread', label: 'Bread', priceUnit: 2.17, sale: 3, priceSale: 6 },
	{ id: 2, value: 'banana', label: 'Banana', priceUnit: 0.99 },
	{ id: 3, value: 'apple', label: 'Apple', priceUnit: 0.89 }
]
