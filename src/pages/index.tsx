import type { NextPage } from 'next'
import Container from '@mui/material/Container'
import { MainLayout } from '@layouts/Main/index'
import { Box, Button, Dialog, IconButton, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material'
import { food, Ifood } from '@mocks/grocery'
import { useState } from 'react'
import ClearIcon from '@mui/icons-material/Clear'
import { uniq } from 'lodash'
import { sleep } from '@utils/sleep'
const Home:NextPage = () => {
	const [list, setList] = useState<Ifood[]>( [] )
	const [open, setOpen] = useState( false )

	const handleSelectedItem = ( item:Ifood ) => {
		setList( ( state ) => [...state, item] )
	}

	const handleRemove = ( i:number ) => {
		const newList = [...list]
		newList.splice( i, 1 )
		setList( newList )
	}

	const count:{[key:string]:number} = {}
	list.forEach( ( i ) => { count[i.value] = ( count[i.value] || 0 ) + 1 } )

	const invoice = uniq( list ).map( ( item ) => {
		const quantityTotal = count[item.value]
		let quantitySale = 0
		let quantityUnit = quantityTotal
		let totalNoSave = 0
		let totalSave = 0
		if ( item.sale ) {
			quantitySale = Math.floor( quantityTotal / item.sale )
			quantityUnit = quantityTotal - ( quantitySale * item.sale )
		}
		totalSave = quantityUnit * item.priceUnit + quantitySale * ( item.priceSale || 0 )
		totalNoSave = quantityTotal * item.priceUnit
		return { ...item, quantityTotal, totalSave, totalNoSave }
	} )

	let totalSave = 0
	let totalNoSave = 0
	invoice.forEach( ( item ) => {
		totalSave = totalSave + item.totalSave
		totalNoSave = totalNoSave + item.totalNoSave
	} )

	return (
		<MainLayout>
			<Container maxWidth="lg">
				<Box sx= { { m: 2 } }>
					<Typography variant="h4" component="h1" gutterBottom>
					Grocery Store
					</Typography>
				</Box>
				<Box sx={ { display: 'flex' } }>
					<Box sx= { { m: 2, width: '200px' } }>
						<Typography variant='h5'>Pick some items</Typography>
						<Box sx={ { display: 'grid', gridTemplateColumns: '1fr', gap: 1 } }>
							{
								food.map( item => (
									<Button key={ item.value } variant='contained' onClick={ () => handleSelectedItem( item ) }>{item.label}</Button>
								) )
							}
							{
								list.length > 0 && (
									<Button sx={ { mt: 3 } } variant='contained' color='success' onClick={ () => setOpen( true ) }>Pay</Button>
								)
							}
						</Box>
					</Box>
					<Box sx= { { m: 2, flexGrow: 1 } }>
						<Typography variant='h5'>Cash counter</Typography>
						<Box sx={ { alignSelf: 'flex-start', display: 'grid', gridTemplateColumns: 'repeat(auto-fill,minmax(120px,1fr))', gap: 1 } }>
							{
								list.map( ( item, i ) => (
									<Box key={ i + item.value } sx={ { p: 1, display: 'flex', justifyContent: 'space-between', alignItems: 'center', border: 'solid 1px', borderColor: ( theme ) => theme.palette.primary.main } }>
										<Typography>{i + 1}</Typography>
										<Typography>{item.label}</Typography>
										<IconButton onClick={ () => handleRemove( i ) } size='small' color='error'><ClearIcon fontSize='small' /></IconButton>
									</Box>
								) )
							}
						</Box>
					</Box>

					<Dialog
						open={ open }
						onClose={ () => setOpen( false ) }
						fullWidth
					>
						<Box sx={ { p: 2 } }>
							<Typography variant='h5'>Invoice</Typography>
							<TableContainer>
								<Table size='small'>
									<TableHead>
										<TableRow>
											<TableCell width='120px'>Item</TableCell>
											<TableCell align='right'>Quantity</TableCell>
											<TableCell align='right'>Price</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{
											invoice.map( ( item, i ) => {
												return (
													<TableRow key={ 'list' + item.value + i } hover>
														<TableCell>{item.label}</TableCell>
														<TableCell align='right'>{item.quantityTotal}</TableCell>
														<TableCell align='right'>${item.totalSave}</TableCell>
													</TableRow>
												)
											} )
										}
										<TableRow>
											<TableCell></TableCell>
											<TableCell align='right'><strong>Total</strong></TableCell>
											<TableCell align="right"><strong>${totalSave.toFixed( 2 )}</strong></TableCell>
										</TableRow>
										<TableRow>
											<TableCell></TableCell>
											<TableCell align='right'><strong>Saved</strong></TableCell>
											<TableCell align="right"><strong>${( totalNoSave - totalSave ).toFixed( 2 )}</strong></TableCell>
										</TableRow>
									</TableBody>
								</Table>
							</TableContainer>
							<Box sx={ { display: 'flex', justifyContent: 'flex-end', alignItems: 'center', mt: 1 } }>
								<Button
									variant='outlined'
									onClick={
										async () => {
											setOpen( false )
											await sleep( 500 )
											setList( [] )
										}
									}>
									Ok
								</Button>
							</Box>
						</Box>
					</Dialog>
				</Box>
			</Container>
		</MainLayout>
	)
}

export default Home
