import { Box } from '@mui/material'
import { FC, ReactNode } from 'react'

export interface IMainLayout {
	children:ReactNode
}

export const MainLayout:FC<IMainLayout> = ( { children } ) => {
	return (
		<Box>
			{children}
		</Box>
	)
}
