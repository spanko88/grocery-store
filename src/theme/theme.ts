import { createTheme } from '@mui/material/styles'

const theme = createTheme( {
	palette: {
		primary: {
			main: '#ff8c00'
		}
	}
} )

export default theme
